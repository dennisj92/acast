import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { ReactComponent as PlayIcon } from '../../SVG/play.svg';

const Button = styled.button`
  border: 3px solid #222;
  background: #fff;
  border-radius: 5px;
  padding: 0;
  outline: none;

  &:hover {
    cursor: pointer;
  }

  svg {
    display: block;
    padding: 0px 10px;
  }
`;

export default class PlayButton extends PureComponent {
  render() {
    return(
      <Button>
        <PlayIcon />
      </Button>
    )
  }
}