import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Play from './UI/PlayButton';
import Forward from './UI/ForwardButton';
import Rewind from './UI/BackButton';

const PlayerControls = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  border-top: 3px solid #222;

  button {
    margin: 7.5px;
    border: 3px solid #999;
  }

  button svg {
    fill: #999;
  }
  
`;

const DisplaySection = styled.section`
  width: 100%
  display: flex;
  flex-direction: column;
  min-height: 300px;
  background: transparent;
  
  p {
    flex-grow: 1;
    text-align: center;
    padding-top: 25%;
    margin: 0;
  }
`;

export default class InactivePlayerControls extends PureComponent {
  render() {
    return (
      <React.Fragment>
        <DisplaySection>
          <p>
            Ad Space Available
          </p>
        </DisplaySection>
        <PlayerControls>
          <Rewind />
          <Play />
          <Forward />
        </PlayerControls>
      </React.Fragment>
    )
  }
}