import React, { PureComponent } from 'react';
import styled from 'styled-components';

const DisplaySection = styled.section`
  width: 100%
  display: flex;
  flex-direction: column;
  min-height: 300px;
  background: transparent;

  > a {
    
    padding-top: 25%;
    width: 100%;
    text-align: center;
  }

  > img {
    display: block;
    width: 100%;
  }
  
  p {
    flex-grow: 1;
    text-align: center;
    padding-top: 25%;
    margin: 0;
  }
`;

export default class Display extends PureComponent {
  render() {
    const currentAd = this.props.data.markers[1];

    return (
      <DisplaySection>
        {
          currentAd.type === "ad" &&
            <a href={currentAd.link}>
              {currentAd.content}
            </a>
        }
        
        {
          currentAd.type === "image" &&
            <img src={'http://localhost:1337/' + currentAd.content} alt={currentAd.content} />
        }

        {
          currentAd.type === "text" &&
            <p>
              {currentAd.content}
            </p>
        }
      </DisplaySection>
    )
  }
}