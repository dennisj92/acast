import React, { Component } from 'react';
import styled from 'styled-components';

import Play from './UI/PlayButton';
import Forward from './UI/ForwardButton';
import Rewind from './UI/BackButton';
import Pause from './UI/PauseButton';

import Display from './Display';

const Player = styled.section`
  width: 100%;
`;

const CurrentTrack = styled.section`
  width: 100%;
`;

const Title = styled.h2`
  width: 100%;
  text-align: center;
`;

const PlayerControls = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  border-top: 3px solid #222;

  button {
    margin: 7.5px;
  }
`;

export default class PodcastPlayer extends Component {
  constructor(props) {
    super(props)

    const { track } = props;

    this.state = {
      playing: false,
      playingEpisode: false,
      playingAd: false,
      currentTime: 0,
      currentTrackData: track,
      updateTimer: null
    }

    this.currentTrackRef = React.createRef()

    this.playTrack = this.playTrack.bind(this)
    this.skipForward = this.skipForward.bind(this)
    this.rewind = this.rewind.bind(this)
    this.updateCurrentTrackPosition = this.updateCurrentTrackPosition.bind(this)
  }

  playTrack() {
    if (this.state.playing) {
      this.currentTrackRef.current.pause()
      clearInterval(this.state.updateTimer)
    } else {
      this.currentTrackRef.current.play()
      let checkCurrentTrackPosition = setInterval(this.updateCurrentTrackPosition, 1000)
      this.setState({ updateTimer: checkCurrentTrackPosition })
    }

    this.setState({ playing: !this.state.playing })
  }

  skipForward() {
    const newTime = this.currentTrackRef.current.currentTime + 5

    if (newTime > this.currentTrackRef.current.duration) {
      this.setState({ playing: false })
      return
    } else {
      this.currentTrackRef.current.currentTime = newTime
    }
  }

  rewind() {
    return this.currentTrackRef.current.currentTime = this.currentTrackRef.current.currentTime - 5
  }

  updateCurrentTrackPosition() {
    if (this.state.playing) {
      this.setState({ currentTime: this.currentTrackRef.current.currentTime })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.track !== prevProps.track) {
      this.currentTrackRef.current.currentTime = 0
      this.currentTrackRef.current.pause()

      clearInterval(this.state.updateTimer)

      this.setState({
        ...this.state,
        currentTrackData: this.props.track,
        playing: !this.state.playing
      })
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.track !== state.currentTrackData) {
      return {
        currentTrackData: props.track,
      };
    }
    return null;
  }

  render() {
    return (
      <Player>

        <CurrentTrack>
          {
            this.state && this.state.currentTrackData &&
            <React.Fragment>
              <audio id="currentTrack" ref={this.currentTrackRef}>
                <source src={'http://localhost:1337' + this.state.currentTrackData.audio} type="audio/mpeg" />
              </audio>

              <Display data={this.state.currentTrackData} />
              <Title>
                {this.state.currentTrackData.name}
              </Title>
            </React.Fragment>
          }
        </CurrentTrack>

        <PlayerControls>
          <div onClick={this.rewind.bind(this)}>
            <Rewind />
          </div>

          <div onClick={this.playTrack.bind(this)}>
            {
              this.state.playing ? <Pause /> : <Play />
            }
          </div>

          <div onClick={this.skipForward.bind(this)}>
            <Forward />
          </div>
        </PlayerControls>

      </Player>
    )
  }
}