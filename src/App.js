import React, { Component } from 'react';
import './App.css';
import Player from './Components/Player';
import InactivePlayerControls from './Components/InactivePlayerControls';
import styled from 'styled-components';

const EpisodeList = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  padding: 0;
  margin: 0;
`;

const EpisodeListItem = styled.li`
  border-bottom: 3px solid #222;
  padding: 10px;
  font-weight: bold;
  letter-spacing: 1px;
  text-transform: capitalize;
  font-family: sans-serif;

  &:hover {
    background-color: #222;
    color: #fff;
    cursor: pointer;
  }
`;

const InnerWrapper = styled.div`
  width: 40%;
  margin: auto;
  display: flex;
  flex-direction: column;
  border: 3px solid #222;
  background: #fff;
  border-radius: 5px;
`;

class App extends Component {
  constructor() {
    super()

    this.state = {
      data: null,
      activeTrack: null
    }

    this.fetchEpisode = this.fetchEpisode.bind(this)
    this.changeTrack = this.changeTrack.bind(this)
  }

  componentWillMount() {
    fetch('http://localhost:1337/episodes')
      .then(response => response.json())
      .then(data => this.setState({ data: data }))
  }

  fetchEpisode(episodeId) {
    const source = 'http://localhost:1337/episodes/' + episodeId

    fetch(source)
      .then(response => response.json())
      .then(data => (
        this.setState({ activeTrack: data })
      ))
  }

  changeTrack(episode) {
    this.fetchEpisode(episode.id)
  }

  render() {
    return (
      <div className="App">
        <InnerWrapper>
          {
            this.state && this.state.data &&
            <EpisodeList>
              {this.state.data.map((item, i) => {
                return (
                  <EpisodeListItem key={item.id} onClick={() => this.changeTrack(item)}>
                    {item.name}
                  </EpisodeListItem>
                )
              })}
            </EpisodeList>
          }
          {
            this.state && this.state.activeTrack
              ?
              <Player track={this.state.activeTrack} />
              :
              <InactivePlayerControls />
          }

        </InnerWrapper>
      </div>
    );
  }
}

export default App;
